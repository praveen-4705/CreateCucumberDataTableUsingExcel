Feature: This is a sample feature file

  Background:
    Given scenario data

  Scenario Outline: This is a scenario to test datadriven test on Cucumber JVM.
    Given scenario data
    When executed from Runner Class.
    Then UserName and Password shows on console from Examples "<UserName>"
    Then UserName and Password shows on console from Examples "<UserName>" and "<Password>"

    Examples:
      | UserName |Password|
      ##@externaldata@./src/test/resources/TestData.xlsx@Sheet1@2,4
